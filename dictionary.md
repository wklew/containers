# dictionary

```scheme
(import (containers dictionary))
```

Finite map structure with integer keys, based on big-endian patricia trees.

Supports efficient lookup and insertion of key-value pairs,
as well as efficient merging (or set union) of multiple dictionaries.

## contents

- [construction](#construction)
  - [empty](#empty)
  - [single](#single)
  - [from alist](#from-alist)
- [key lookup](#key-lookup)
  - [lookup](#lookup)
  - [minimum and maximum](#minimum-and-maximum)
- [insertion and removal](#insertion-and-removal)
  - [insert](#insert)
  - [delete](#delete)
  - [decompose](#decompose)
- [merging](#merging)
  - [merge](#merge)
- [mapping](#mapping)
  - [map](#map)
- [folding](#folding)
  - [fold](#fold)
  - [to alist](#to-alist)

# construction

## empty

```scheme
(dictionary-empty)
```

_O(1)._
Create an empty dictionary.

## single

```scheme
(dictionary-single key val)
```

_O(1)._
Create a singleton dictionary.

## from alist

```scheme
(alist->dictionary alist)
(alist->dictionary combine alist)
```

_O(n*min(n,W))._
Create a dictionary from `alist`, a list of key-value pairs.
By default, elements on the left are replaced
by elements on the right at the same key.
If `combine` is given, instead combine the old and new values
by calling `(combine old val)`, where `old` is the old value.

# key lookup

## lookup

```scheme
(dictionary-lookup key dict)
```

_O(min(n,W))._
Retrieve the value associated with `key` in `dict`.
If `dict` is empty, raise an exception satisfying `dictionary-lookup-error?`.

## minimum and maximum

```scheme
(dictionary-minimum dict)
(dictionary-maximum dict)
```

_O(min(n,W))._
Retrieve the minimum or maximum key stored in `dict`.
Return the key and associated value as two values.
If `dict` is empty, raise an exception satisfying `dictionary-lookup-error?`.

# insertion and removal

## insert

```scheme
(dictionary-insert key val dict)
(dictionary-insert combine key val dict)
(dictionary-insert-pair key/val dict)
(dictionary-insert-pair combine key/val dict)
```

_O(min(n,W))._
Insert `val` into `dict` and associate it with `key`.
Return the new dictionary.
By default, old values are replaced by new values at the same key.
If `combine` is given, instead combine the old and new values
by calling `(combine old val)`, where `old` is the old value.

`dictionary-insert-pair` is provided for the common case of inserting pairs.
It is also slightly faster,
since dictionary entries are stored as pairs internally.

## delete

```scheme
(dictionary-delete key dict)
```

_O(min(n,W))._
Delete the value associated with `key` in `dict`.
Return the new dictionary.
If no value is associated with `key`, return the dictionary unchanged.

## decompose

```scheme
(dictionary-decompose key dict)
```

_O(min(n,W))._
Lookup and delete the value associated with `key` in `dict`.
Return the retrieved value and the new dictionary as two values.
If no value is associated with `key`, raise a `&dictionary-lookup-error`.

Equivalent to the following, but requires only one tree traversal:

```scheme
(let ((val (dictionary-lookup key dict)))
  (values val (dictionary-delete key dict)))
```

# merging

## merge

```scheme
(dictionary-merge . dicts)
(dictionary-merge-with combine . dicts)
(dictionary-merge-list dicts)
(dictionary-merge-list combine dicts)
```

_O(n+m)._
Perform the set union of `dicts` and return the combined dictionary.
By default, elements on the left are replaced
by elements on the right at the same key.
If `combine` is provided, instead combine the old and new values
by calling `(combine old val)`, where `old` is the old value.

Multiple versions are provided to support different calling conventions.

# mapping

## map

```scheme
(dictionary-map proc dict)
```

_O(n)._
Map over values in `dict`, applying `proc` to each element.
Return the new dictionary.

# folding

## fold

```scheme
(dictionary-foldl insert base dict)
(dictionary-foldr insert base dict)
```

_O(n)._
Fold over values in `dict`, in ascending or descending order,
using an insertion function and base value.
Return the final value accumulated by the fold.

The insertion function is called as `(insert key val acc`),
where `key` and `val` are the key and value of the current element,
and `acc` is the value accumulated by the fold thus far.

```scheme
(define dict (alist->dictionary '((2 . a) (3 . b) (0 . c))))
(dictionary-foldl list '() dict)
;; => (3 b (2 a (0 c ())))
```

## to alist

```scheme
(dictionary->ascending-alist dict)
(dictionary->descending-alist dict)
```

_O(n)._
Convert `dict` to an alist, in ascending or descending order.

Example implementation:

```scheme
(define (acons k v acc)
  (cons (cons k v) acc))

(define (dictionary->ascending-alist dict)
  (dictionary-foldr acons '() dict))
```
