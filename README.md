# containers

R7RS Scheme implementations of commonly used functional data structures.
Inspired by Scheme's [`pfds`](https://github.com/ijp/pfds)
and Haskell's [`containers`](https://hackage.haskell.org/package/containers).

These containers are thread safe and preserve sharing,
meaning different versions of the same structure
can be manipulated in parallel safely and without overhead.

Work in progress.

## sequence

`(import (containers sequence))`

Simple sequence or catenable deque (double-ended queue),
based on finger trees with lazily computed inner nodes.

Supports linear access at either end, as well as fast appends.

API reference is in the [sequence readme file](sequence.md).

## dictionary

`(import (containers dictionary))`

Finite map structure with integer keys, based on big-endian patricia trees.

Supports fast lookup and insertion of key-value pairs,
as well as efficient set operations for combining dictionaries
such as union and intersection.

API reference is in the [dictionary readme file](dictionary.md).
