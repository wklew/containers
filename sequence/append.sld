;;; This module was generated dynamically from
;;; containers/sequence/generate-append.scm
;;; using Scheme implementation: guile

(define-library
  (containers sequence append)
  (export %sequence-append)
  (import
    (scheme base)
    (scheme lazy)
    (containers tree)
    (containers sequence internal))
  (begin
    (define appender0
      (lambda ()
        (lambda (s0 s1)
          (tree-case2
            s0
            s1
            (lambda (s1) s1)
            (lambda (s0) s0)
            (lambda (x s1) (sequence-cons x s1))
            (lambda (x s0) (sequence-snoc x s0))
            (lambda (l l0 l1 r r0 r1)
              (let ((add (adder0 l1 r0)))
                (make-tree-deep
                  (delay (add (force l) (force r)))
                  l0
                  r1)))))))
    (define appender1
      (lambda (z1)
        (lambda (s0 s1)
          (tree-case2
            s0
            s1
            (lambda (s1) (sequence-cons z1 s1))
            (lambda (s0) (sequence-snoc z1 s0))
            (lambda (x s1)
              (sequence-cons x (sequence-cons z1 s1)))
            (lambda (x s0)
              (sequence-snoc z1 (sequence-snoc x s0)))
            (lambda (l l0 l1 r r0 r1)
              (let ((add (adder1 l1 r0 z1)))
                (make-tree-deep
                  (delay (add (force l) (force r)))
                  l0
                  r1)))))))
    (define appender2
      (lambda (z1 z2)
        (lambda (s0 s1)
          (tree-case2
            s0
            s1
            (lambda (s1)
              (sequence-cons z1 (sequence-cons z2 s1)))
            (lambda (s0)
              (sequence-snoc z2 (sequence-snoc z1 s0)))
            (lambda (x s1)
              (sequence-cons
                x
                (sequence-cons z1 (sequence-cons z2 s1))))
            (lambda (x s0)
              (sequence-snoc
                z2
                (sequence-snoc z1 (sequence-snoc x s0))))
            (lambda (l l0 l1 r r0 r1)
              (let ((add (adder2 l1 r0 z1 z2)))
                (make-tree-deep
                  (delay (add (force l) (force r)))
                  l0
                  r1)))))))
    (define appender3
      (lambda (z1 z2 z3)
        (lambda (s0 s1)
          (tree-case2
            s0
            s1
            (lambda (s1)
              (sequence-cons
                z1
                (sequence-cons z2 (sequence-cons z3 s1))))
            (lambda (s0)
              (sequence-snoc
                z3
                (sequence-snoc z2 (sequence-snoc z1 s0))))
            (lambda (x s1)
              (sequence-cons
                x
                (sequence-cons
                  z1
                  (sequence-cons z2 (sequence-cons z3 s1)))))
            (lambda (x s0)
              (sequence-snoc
                z3
                (sequence-snoc
                  z2
                  (sequence-snoc z1 (sequence-snoc x s0)))))
            (lambda (l l0 l1 r r0 r1)
              (let ((add (adder3 l1 r0 z1 z2 z3)))
                (make-tree-deep
                  (delay (add (force l) (force r)))
                  l0
                  r1)))))))
    (define appender4
      (lambda (z1 z2 z3 z4)
        (lambda (s0 s1)
          (tree-case2
            s0
            s1
            (lambda (s1)
              (sequence-cons
                z1
                (sequence-cons
                  z2
                  (sequence-cons z3 (sequence-cons z4 s1)))))
            (lambda (s0)
              (sequence-snoc
                z4
                (sequence-snoc
                  z3
                  (sequence-snoc z2 (sequence-snoc z1 s0)))))
            (lambda (x s1)
              (sequence-cons
                x
                (sequence-cons
                  z1
                  (sequence-cons
                    z2
                    (sequence-cons z3 (sequence-cons z4 s1))))))
            (lambda (x s0)
              (sequence-snoc
                z4
                (sequence-snoc
                  z3
                  (sequence-snoc
                    z2
                    (sequence-snoc z1 (sequence-snoc x s0))))))
            (lambda (l l0 l1 r r0 r1)
              (let ((add (adder4 l1 r0 z1 z2 z3 z4)))
                (make-tree-deep
                  (delay (add (force l) (force r)))
                  l0
                  r1)))))))
    (define adder0
      (lambda (x y)
        (digit-case
          x
          (lambda (x1)
            (digit-case
              y
              (lambda (y1) (appender1 (vector x1 y1)))
              (lambda (y1 y2) (appender1 (vector x1 y1 y2)))
              (lambda (y1 y2 y3)
                (appender2 (vector x1 y1) (vector y2 y3)))
              (lambda (y1 y2 y3 y4)
                (appender2 (vector x1 y1 y2) (vector y3 y4)))))
          (lambda (x1 x2)
            (digit-case
              y
              (lambda (y1) (appender1 (vector x1 x2 y1)))
              (lambda (y1 y2)
                (appender2 (vector x1 x2) (vector y1 y2)))
              (lambda (y1 y2 y3)
                (appender2 (vector x1 x2 y1) (vector y2 y3)))
              (lambda (y1 y2 y3 y4)
                (appender2 (vector x1 x2 y1) (vector y2 y3 y4)))))
          (lambda (x1 x2 x3)
            (digit-case
              y
              (lambda (y1)
                (appender2 (vector x1 x2) (vector x3 y1)))
              (lambda (y1 y2)
                (appender2 (vector x1 x2 x3) (vector y1 y2)))
              (lambda (y1 y2 y3)
                (appender2 (vector x1 x2 x3) (vector y1 y2 y3)))
              (lambda (y1 y2 y3 y4)
                (appender3
                  (vector x1 x2 x3)
                  (vector y1 y2)
                  (vector y3 y4)))))
          (lambda (x1 x2 x3 x4)
            (digit-case
              y
              (lambda (y1)
                (appender2 (vector x1 x2 x3) (vector x4 y1)))
              (lambda (y1 y2)
                (appender2 (vector x1 x2 x3) (vector x4 y1 y2)))
              (lambda (y1 y2 y3)
                (appender3
                  (vector x1 x2 x3)
                  (vector x4 y1)
                  (vector y2 y3)))
              (lambda (y1 y2 y3 y4)
                (appender3
                  (vector x1 x2 x3)
                  (vector x4 y1 y2)
                  (vector y3 y4))))))))
    (define adder1
      (lambda (x y z1)
        (digit-case
          x
          (lambda (x1)
            (digit-case
              y
              (lambda (y1) (appender1 (vector x1 z1 y1)))
              (lambda (y1 y2)
                (appender2 (vector x1 z1) (vector y1 y2)))
              (lambda (y1 y2 y3)
                (appender2 (vector x1 z1 y1) (vector y2 y3)))
              (lambda (y1 y2 y3 y4)
                (appender2 (vector x1 z1 y1) (vector y2 y3 y4)))))
          (lambda (x1 x2)
            (digit-case
              y
              (lambda (y1)
                (appender2 (vector x1 x2) (vector z1 y1)))
              (lambda (y1 y2)
                (appender2 (vector x1 x2 z1) (vector y1 y2)))
              (lambda (y1 y2 y3)
                (appender2 (vector x1 x2 z1) (vector y1 y2 y3)))
              (lambda (y1 y2 y3 y4)
                (appender3
                  (vector x1 x2 z1)
                  (vector y1 y2)
                  (vector y3 y4)))))
          (lambda (x1 x2 x3)
            (digit-case
              y
              (lambda (y1)
                (appender2 (vector x1 x2 x3) (vector z1 y1)))
              (lambda (y1 y2)
                (appender2 (vector x1 x2 x3) (vector z1 y1 y2)))
              (lambda (y1 y2 y3)
                (appender3
                  (vector x1 x2 x3)
                  (vector z1 y1)
                  (vector y2 y3)))
              (lambda (y1 y2 y3 y4)
                (appender3
                  (vector x1 x2 x3)
                  (vector z1 y1 y2)
                  (vector y3 y4)))))
          (lambda (x1 x2 x3 x4)
            (digit-case
              y
              (lambda (y1)
                (appender2 (vector x1 x2 x3) (vector x4 z1 y1)))
              (lambda (y1 y2)
                (appender3
                  (vector x1 x2 x3)
                  (vector x4 z1)
                  (vector y1 y2)))
              (lambda (y1 y2 y3)
                (appender3
                  (vector x1 x2 x3)
                  (vector x4 z1 y1)
                  (vector y2 y3)))
              (lambda (y1 y2 y3 y4)
                (appender3
                  (vector x1 x2 x3)
                  (vector x4 z1 y1)
                  (vector y2 y3 y4))))))))
    (define adder2
      (lambda (x y z1 z2)
        (digit-case
          x
          (lambda (x1)
            (digit-case
              y
              (lambda (y1)
                (appender2 (vector x1 z1) (vector z2 y1)))
              (lambda (y1 y2)
                (appender2 (vector x1 z1 z2) (vector y1 y2)))
              (lambda (y1 y2 y3)
                (appender2 (vector x1 z1 z2) (vector y1 y2 y3)))
              (lambda (y1 y2 y3 y4)
                (appender3
                  (vector x1 z1 z2)
                  (vector y1 y2)
                  (vector y3 y4)))))
          (lambda (x1 x2)
            (digit-case
              y
              (lambda (y1)
                (appender2 (vector x1 x2 z1) (vector z2 y1)))
              (lambda (y1 y2)
                (appender2 (vector x1 x2 z1) (vector z2 y1 y2)))
              (lambda (y1 y2 y3)
                (appender3
                  (vector x1 x2 z1)
                  (vector z2 y1)
                  (vector y2 y3)))
              (lambda (y1 y2 y3 y4)
                (appender3
                  (vector x1 x2 z1)
                  (vector z2 y1 y2)
                  (vector y3 y4)))))
          (lambda (x1 x2 x3)
            (digit-case
              y
              (lambda (y1)
                (appender2 (vector x1 x2 x3) (vector z1 z2 y1)))
              (lambda (y1 y2)
                (appender3
                  (vector x1 x2 x3)
                  (vector z1 z2)
                  (vector y1 y2)))
              (lambda (y1 y2 y3)
                (appender3
                  (vector x1 x2 x3)
                  (vector z1 z2 y1)
                  (vector y2 y3)))
              (lambda (y1 y2 y3 y4)
                (appender3
                  (vector x1 x2 x3)
                  (vector z1 z2 y1)
                  (vector y2 y3 y4)))))
          (lambda (x1 x2 x3 x4)
            (digit-case
              y
              (lambda (y1)
                (appender3
                  (vector x1 x2 x3)
                  (vector x4 z1)
                  (vector z2 y1)))
              (lambda (y1 y2)
                (appender3
                  (vector x1 x2 x3)
                  (vector x4 z1 z2)
                  (vector y1 y2)))
              (lambda (y1 y2 y3)
                (appender3
                  (vector x1 x2 x3)
                  (vector x4 z1 z2)
                  (vector y1 y2 y3)))
              (lambda (y1 y2 y3 y4)
                (appender4
                  (vector x1 x2 x3)
                  (vector x4 z1 z2)
                  (vector y1 y2)
                  (vector y3 y4))))))))
    (define adder3
      (lambda (x y z1 z2 z3)
        (digit-case
          x
          (lambda (x1)
            (digit-case
              y
              (lambda (y1)
                (appender2 (vector x1 z1 z2) (vector z3 y1)))
              (lambda (y1 y2)
                (appender2 (vector x1 z1 z2) (vector z3 y1 y2)))
              (lambda (y1 y2 y3)
                (appender3
                  (vector x1 z1 z2)
                  (vector z3 y1)
                  (vector y2 y3)))
              (lambda (y1 y2 y3 y4)
                (appender3
                  (vector x1 z1 z2)
                  (vector z3 y1 y2)
                  (vector y3 y4)))))
          (lambda (x1 x2)
            (digit-case
              y
              (lambda (y1)
                (appender2 (vector x1 x2 z1) (vector z2 z3 y1)))
              (lambda (y1 y2)
                (appender3
                  (vector x1 x2 z1)
                  (vector z2 z3)
                  (vector y1 y2)))
              (lambda (y1 y2 y3)
                (appender3
                  (vector x1 x2 z1)
                  (vector z2 z3 y1)
                  (vector y2 y3)))
              (lambda (y1 y2 y3 y4)
                (appender3
                  (vector x1 x2 z1)
                  (vector z2 z3 y1)
                  (vector y2 y3 y4)))))
          (lambda (x1 x2 x3)
            (digit-case
              y
              (lambda (y1)
                (appender3
                  (vector x1 x2 x3)
                  (vector z1 z2)
                  (vector z3 y1)))
              (lambda (y1 y2)
                (appender3
                  (vector x1 x2 x3)
                  (vector z1 z2 z3)
                  (vector y1 y2)))
              (lambda (y1 y2 y3)
                (appender3
                  (vector x1 x2 x3)
                  (vector z1 z2 z3)
                  (vector y1 y2 y3)))
              (lambda (y1 y2 y3 y4)
                (appender4
                  (vector x1 x2 x3)
                  (vector z1 z2 z3)
                  (vector y1 y2)
                  (vector y3 y4)))))
          (lambda (x1 x2 x3 x4)
            (digit-case
              y
              (lambda (y1)
                (appender3
                  (vector x1 x2 x3)
                  (vector x4 z1 z2)
                  (vector z3 y1)))
              (lambda (y1 y2)
                (appender3
                  (vector x1 x2 x3)
                  (vector x4 z1 z2)
                  (vector z3 y1 y2)))
              (lambda (y1 y2 y3)
                (appender4
                  (vector x1 x2 x3)
                  (vector x4 z1 z2)
                  (vector z3 y1)
                  (vector y2 y3)))
              (lambda (y1 y2 y3 y4)
                (appender4
                  (vector x1 x2 x3)
                  (vector x4 z1 z2)
                  (vector z3 y1 y2)
                  (vector y3 y4))))))))
    (define adder4
      (lambda (x y z1 z2 z3 z4)
        (digit-case
          x
          (lambda (x1)
            (digit-case
              y
              (lambda (y1)
                (appender2 (vector x1 z1 z2) (vector z3 z4 y1)))
              (lambda (y1 y2)
                (appender3
                  (vector x1 z1 z2)
                  (vector z3 z4)
                  (vector y1 y2)))
              (lambda (y1 y2 y3)
                (appender3
                  (vector x1 z1 z2)
                  (vector z3 z4 y1)
                  (vector y2 y3)))
              (lambda (y1 y2 y3 y4)
                (appender3
                  (vector x1 z1 z2)
                  (vector z3 z4 y1)
                  (vector y2 y3 y4)))))
          (lambda (x1 x2)
            (digit-case
              y
              (lambda (y1)
                (appender3
                  (vector x1 x2 z1)
                  (vector z2 z3)
                  (vector z4 y1)))
              (lambda (y1 y2)
                (appender3
                  (vector x1 x2 z1)
                  (vector z2 z3 z4)
                  (vector y1 y2)))
              (lambda (y1 y2 y3)
                (appender3
                  (vector x1 x2 z1)
                  (vector z2 z3 z4)
                  (vector y1 y2 y3)))
              (lambda (y1 y2 y3 y4)
                (appender4
                  (vector x1 x2 z1)
                  (vector z2 z3 z4)
                  (vector y1 y2)
                  (vector y3 y4)))))
          (lambda (x1 x2 x3)
            (digit-case
              y
              (lambda (y1)
                (appender3
                  (vector x1 x2 x3)
                  (vector z1 z2 z3)
                  (vector z4 y1)))
              (lambda (y1 y2)
                (appender3
                  (vector x1 x2 x3)
                  (vector z1 z2 z3)
                  (vector z4 y1 y2)))
              (lambda (y1 y2 y3)
                (appender4
                  (vector x1 x2 x3)
                  (vector z1 z2 z3)
                  (vector z4 y1)
                  (vector y2 y3)))
              (lambda (y1 y2 y3 y4)
                (appender4
                  (vector x1 x2 x3)
                  (vector z1 z2 z3)
                  (vector z4 y1 y2)
                  (vector y3 y4)))))
          (lambda (x1 x2 x3 x4)
            (digit-case
              y
              (lambda (y1)
                (appender3
                  (vector x1 x2 x3)
                  (vector x4 z1 z2)
                  (vector z3 z4 y1)))
              (lambda (y1 y2)
                (appender4
                  (vector x1 x2 x3)
                  (vector x4 z1 z2)
                  (vector z3 z4)
                  (vector y1 y2)))
              (lambda (y1 y2 y3)
                (appender4
                  (vector x1 x2 x3)
                  (vector x4 z1 z2)
                  (vector z3 z4 y1)
                  (vector y2 y3)))
              (lambda (y1 y2 y3 y4)
                (appender4
                  (vector x1 x2 x3)
                  (vector x4 z1 z2)
                  (vector z3 z4 y1)
                  (vector y2 y3 y4))))))))
    (define %sequence-append (appender0))))
