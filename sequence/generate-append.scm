;;; generate-append.scm --- Generate internal sequence-append definitions

(import (scheme base)
        (scheme write)
        (scheme file))

(cond-expand
 (guile (import (ice-9 pretty-print)
                (srfi srfi-1))
        (begin
          (define (pretty x) x)
          (define (show dest x) (pretty-print x dest))
          (define implementation 'guile)))
 (chibi (import (srfi 1)
                (srfi 166))
        (begin
          (define implementation 'chibi))))

;; Helpers for generating names

(define (iota start stop)
  (unfold (lambda (n) (> n stop))
          (lambda (n) n)
          (lambda (n) (+ n 1))
          start))

(define (name-generator base)
  (let ((str (symbol->string base)))
    (lambda (n)
      (string->symbol (string-append str (number->string n))))))

(define (gen-names base start stop)
  (map (name-generator base)
       (iota start stop)))

;; Static names for the specialized append and add functions

(define appender-names
  (list->vector (gen-names 'appender 0 4)))

(define adder-names
  (list->vector (gen-names 'adder 0 4)))

(define (appender-name n)
  (vector-ref appender-names n))

(define (adder-name n)
  (vector-ref adder-names n))

;; Encodings for the numbers 2 through 12 as 2-3 nodes

(define node-encodings
  '#((2) (3) (2 2) (3 2) (3 3) (3 2 2) (3 3 2)
     (3 3 3) (3 3 2 2) (3 3 3 2) (3 3 3 3)))

(define (node-encoding n)
  (vector-ref node-encodings (- n 2)))

;; Group a list of variables into a sequence of 2-3 nodes

(define (gen-nodes! vars ns)
  (reverse
   (fold (lambda (n acc)
           (let ((node-vars (take vars n)))
             (set! vars (drop vars n))
             `((vector . ,node-vars) . ,acc)))
         '()
         ns)))

;; Group variables into nodes and call the appropriate append

(define (gen-appender-call vars)
  (let ((enc (node-encoding (length vars))))
    (cons (appender-name (length enc))
          (gen-nodes! vars enc))))

;; Generate a sequence of repeated calls to cons/snoc

(define (gen-cons vars lst)
  (fold-right (lambda (x acc) `(sequence-cons ,x ,acc)) vars lst))

(define (gen-snoc vars lst)
  (fold (lambda (x acc) `(sequence-snoc ,x ,acc)) vars lst))

;; Generate an append function specialized to a temporary buffer size

(define (gen-appender buf-len)
  `(define ,(appender-name buf-len)
     ,(let ((buf-vars (gen-names 'z 1 buf-len)))
        `(lambda ,buf-vars
           (lambda (s0 s1)
             (tree-case2 s0 s1
               (lambda (s1)
                 ,(gen-cons 's1 buf-vars))
               (lambda (s0)
                 ,(gen-snoc 's0 buf-vars))
               (lambda (x s1)
                 ,(gen-cons 's1 (cons 'x buf-vars)))
               (lambda (x s0)
                 ,(gen-snoc 's0 (cons 'x buf-vars)))
               (lambda (l l0 l1 r r0 r1)
                 ,(let ((rator (adder-name buf-len)))
                    ;; Evaluate the rest of the append strictly, for better performance
                    `(let ((add (,rator l1 r0 . ,buf-vars)))
                       (make-tree-deep (delay (add (force l) (force r)))
                                       l0
                                       r1))))))))))

;; Fold over the possible cases for a digit (1-4)
;; Note the default `pair-fold` from SRFI-1 does things in reverse

(define (gen-digit-cases var combine)
  (pair-fold (lambda (lst acc)
               (combine (reverse lst) acc))
             '()
             (reverse (gen-names var 1 4))))

;; Generate a digit add function specialized to a buffer size

(define (gen-adder buf-len)
  `(define ,(adder-name buf-len)
     ,(let ((buf-vars (gen-names 'z 1 buf-len)))
        `(lambda (x y . ,buf-vars)
           (digit-case x
             . ,(gen-digit-cases
                 'x
                 (lambda (x-vars x-cases)
                   `((lambda ,x-vars
                       (digit-case y
                         . ,(gen-digit-cases
                             'y
                             (lambda (y-vars y-cases)
                               `((lambda ,y-vars
                                   ,(gen-appender-call
                                     (append x-vars buf-vars y-vars)))
                                 . ,y-cases)))))
                     . ,x-cases))))))))

;; Generate the full module

(define append-module
  `(define-library (containers sequence append)
     (export %sequence-append)
     (import (scheme base)
             (scheme lazy)
             (containers tree)
             (containers sequence internal))
     (begin
       ,@(map gen-appender (iota 0 4))
       ,@(map gen-adder (iota 0 4))
       (define %sequence-append (,(appender-name 0))))))

;; Write the module to a file

(with-output-to-file "containers/sequence/append.sld"
  (lambda ()
    (display ";;; This module was generated dynamically from")
    (newline)
    (display ";;; containers/sequence/generate-append.scm")
    (newline)
    (display ";;; using Scheme implementation: ")
    (write implementation)
    (newline)
    (newline)
    (show (current-output-port)
          (pretty append-module))))
