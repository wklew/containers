;;; sequence/internal.sld --- Internal sequence definitions

;;; Copyright © 2021, 2022 Walter Lewis <wklew@mailbox.org>
;;;
;;; This file is part of containers.
;;;
;;; Containers is free software; you can redistribute it and/or modify it
;;; under the terms of the GNU General Public License as published by
;;; the Free Software Foundation; either version 3 of the License, or (at
;;; your option) any later version.
;;;
;;; Containers is distributed in the hope that it will be useful, but
;;; WITHOUT ANY WARRANTY; without even the implied warranty of
;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;;; GNU General Public License for more details.
;;;
;;; You should have received a copy of the GNU General Public License
;;; along with containers.  If not, see <http://www.gnu.org/licenses/>.

;;; Commentary:
;;;
;;; Implementation taken from the following sources:
;;;
;;; - Ralf Hinze and Ross Paterson,
;;;   "Finger trees: a simple general-purpose data structure",
;;;   Journal of Functional Programming 16:2 (2006) pp 197-217.
;;;
;;; - Haskell's Data.Seq (Ross Paterson et al.)

(define-library (containers sequence internal)

  (export node-case node-foldl node-foldr node-map
          digit-case digit-foldl digit-foldr digit-map
          digit-cons digit-snoc digit-uncons digit-unsnoc
          sequence?
          sequence-empty
          sequence-empty?
          sequence-empty-error?
          sequence-single
          sequence-cons
          sequence-snoc
          sequence-uncons
          sequence-unsnoc
          sequence-foldl
          sequence-foldr
          sequence-map
          list->sequence
          make-sequence
          reverse-list->sequence
          sequence->list
          sequence->reverse-list
          sequence-take
          sequence-drop
          sequence-length)

  (import (scheme base)
          (scheme lazy)
          (containers tree))

  (cond-expand
   (guile (import (srfi srfi-1)))
   (chibi (import (srfi 1))))

  (begin

    ;; internal nodes of 2-3 elements

    (define (node-case t f2 f3)
      (if (= 2 (vector-length t))
          (f2 (vector-ref t 0)
              (vector-ref t 1))
          (f3 (vector-ref t 0)
              (vector-ref t 1)
              (vector-ref t 2))))

    (define (node-foldl c z t)
      (node-case t
        (lambda (v0 v1)
          (c v1 (c v0 z)))
        (lambda (v0 v1 v2)
          (c v2 (c v1 (c v0 z))))))

    (define (node-foldr c z t)
      (node-case t
        (lambda (v0 v1)
          (c v0 (c v1 z)))
        (lambda (v0 v1 v2)
          (c v0 (c v1 (c v2 z))))))

    (define (node-map f t)
      (node-case t
        (lambda (v0 v1)
          (vector (f v0)
                  (f v1)))
        (lambda (v0 v1 v2)
          (vector (f v0)
                  (f v1)
                  (f v2)))))

    ;; exterior digits of 1-4 elements

    (define (digit-case t f1 f2 f3 f4)
      (let ((n (vector-length t)))
        (cond ((= n 1)
               (f1 (vector-ref t 0)))
              ((= n 2)
               (f2 (vector-ref t 0)
                   (vector-ref t 1)))
              ((= n 3)
               (f3 (vector-ref t 0)
                   (vector-ref t 1)
                   (vector-ref t 2)))
              (else
               (f4 (vector-ref t 0)
                   (vector-ref t 1)
                   (vector-ref t 2)
                   (vector-ref t 3))))))

    (define (digit-foldl c z t)
      (digit-case t
        (lambda (v0)
          (c v0 z))
        (lambda (v0 v1)
          (c v1 (c v0 z)))
        (lambda (v0 v1 v2)
          (c v2 (c v1 (c v0 z))))
        (lambda (v0 v1 v2 v3)
          (c v3 (c v2 (c v1 (c v0 z)))))))

    (define (digit-foldr c z t)
      (digit-case t
        (lambda (v0)
          (c v0 z))
        (lambda (v0 v1)
          (c v0 (c v1 z)))
        (lambda (v0 v1 v2)
          (c v0 (c v1 (c v2 z))))
        (lambda (v0 v1 v2 v3)
          (c v0 (c v1 (c v2 (c v3 z)))))))

    (define (digit-map f t)
      (digit-case t
        (lambda (v0)
          (vector (f v0)))
        (lambda (v0 v1)
          (vector (f v0) (f v1)))
        (lambda (v0 v1 v2)
          (vector (f v0) (f v1) (f v2)))
        (lambda (v0 v1 v2 v3)
          (vector (f v0) (f v1) (f v2) (f v3)))))

    (define (digit-cons x t)
      (digit-case t
        (lambda (v0)
          (values (vector x v0)
                  #f))
        (lambda (v0 v1)
          (values (vector x v0 v1)
                  #f))
        (lambda (v0 v1 v2)
          (values (vector x v0 v1 v2)
                  #f))
        (lambda (v0 v1 v2 v3)
          (values (vector x v0)
                  (vector v1 v2 v3)))))

    (define (digit-snoc x t)
      (digit-case t
        (lambda (v0)
          (values (vector v0 x)
                  #f))
        (lambda (v0 v1)
          (values (vector v0 v1 x)
                  #f))
        (lambda (v0 v1 v2)
          (values (vector v0 v1 v2 x)
                  #f))
        (lambda (v0 v1 v2 v3)
          (values (vector v3 x)
                  (vector v0 v1 v2)))))

    (define (digit-uncons t)
      (digit-case t
        (lambda (v0)
          (values v0 #f))
        (lambda (v0 v1)
          (values v0 (vector v1)))
        (lambda (v0 v1 v2)
          (values v0 (vector v1 v2)))
        (lambda (v0 v1 v2 v3)
          (values v0 (vector v1 v2 v3)))))

    (define (digit-unsnoc t)
      (digit-case t
        (lambda (v0)
          (values v0 #f))
        (lambda (v0 v1)
          (values v1 (vector v0)))
        (lambda (v0 v1 v2)
          (values v2 (vector v0 v1)))
        (lambda (v0 v1 v2 v3)
          (values v3 (vector v0 v1 v2)))))

    ;; internal nodes of 2-3 elements

    (define (node-case t f2 f3)
      (if (= 2 (vector-length t))
          (f2 (vector-ref t 0)
              (vector-ref t 1))
          (f3 (vector-ref t 0)
              (vector-ref t 1)
              (vector-ref t 2))))

    (define (node-foldr c z t)
      (node-case t
        (lambda (v0 v1)
          (c v0 (c v1 z)))
        (lambda (v0 v1 v2)
          (c v0 (c v1 (c v2 z))))))

    (define (node-foldl c z t)
      (node-case t
        (lambda (v0 v1)
          (c v1 (c v0 z)))
        (lambda (v0 v1 v2)
          (c v2 (c v1 (c v0 z))))))

    (define (node-map f t)
      (node-case t
        (lambda (v0 v1)
          (vector (f v0)
                  (f v1)))
        (lambda (v0 v1 v2)
          (vector (f v0)
                  (f v1)
                  (f v2)))))

    ;; exterior digits of 1-4 elements

    (define (digit-case t f1 f2 f3 f4)
      (let ((n (vector-length t)))
        (cond ((= n 1)
               (f1 (vector-ref t 0)))
              ((= n 2)
               (f2 (vector-ref t 0)
                   (vector-ref t 1)))
              ((= n 3)
               (f3 (vector-ref t 0)
                   (vector-ref t 1)
                   (vector-ref t 2)))
              (else
               (f4 (vector-ref t 0)
                   (vector-ref t 1)
                   (vector-ref t 2)
                   (vector-ref t 3))))))

    (define (digit-foldl c z t)
      (digit-case t
        (lambda (v0)
          (c v0 z))
        (lambda (v0 v1)
          (c v1 (c v0 z)))
        (lambda (v0 v1 v2)
          (c v2 (c v1 (c v0 z))))
        (lambda (v0 v1 v2 v3)
          (c v3 (c v2 (c v1 (c v0 z)))))))

    (define (digit-foldr c z t)
      (digit-case t
        (lambda (v0)
          (c v0 z))
        (lambda (v0 v1)
          (c v0 (c v1 z)))
        (lambda (v0 v1 v2)
          (c v0 (c v1 (c v2 z))))
        (lambda (v0 v1 v2 v3)
          (c v0 (c v1 (c v2 (c v3 z)))))))

    (define (digit-map f t)
      (digit-case t
        (lambda (v0)
          (vector (f v0)))
        (lambda (v0 v1)
          (vector (f v0) (f v1)))
        (lambda (v0 v1 v2)
          (vector (f v0) (f v1) (f v2)))
        (lambda (v0 v1 v2 v3)
          (vector (f v0) (f v1) (f v2) (f v3)))))

    (define (digit-cons x t)
      (digit-case t
        (lambda (v0)
          (values (vector x v0)
                  #f))
        (lambda (v0 v1)
          (values (vector x v0 v1)
                  #f))
        (lambda (v0 v1 v2)
          (values (vector x v0 v1 v2)
                  #f))
        (lambda (v0 v1 v2 v3)
          (values (vector x v0)
                  (vector v1 v2 v3)))))

    (define (digit-snoc x t)
      (digit-case t
        (lambda (v0)
          (values (vector v0 x)
                  #f))
        (lambda (v0 v1)
          (values (vector v0 v1 x)
                  #f))
        (lambda (v0 v1 v2)
          (values (vector v0 v1 v2 x)
                  #f))
        (lambda (v0 v1 v2 v3)
          (values (vector v3 x)
                  (vector v0 v1 v2)))))

    (define (digit-uncons t)
      (digit-case t
        (lambda (v0)
          (values v0 #f))
        (lambda (v0 v1)
          (values v0 (vector v1)))
        (lambda (v0 v1 v2)
          (values v0 (vector v1 v2)))
        (lambda (v0 v1 v2 v3)
          (values v0 (vector v1 v2 v3)))))

    (define (digit-unsnoc t)
      (digit-case t
        (lambda (v0)
          (values v0 #f))
        (lambda (v0 v1)
          (values v1 (vector v0)))
        (lambda (v0 v1 v2)
          (values v2 (vector v0 v1)))
        (lambda (v0 v1 v2 v3)
          (values v3 (vector v0 v1 v2)))))

    ;; sequences proper

    (define sequence-empty make-tree-nil)
    (define sequence-empty? tree-nil?)

    (define sequence-single make-tree-leaf)

    (define sequence? tree?)

    (define (sequence-foldl c z s)
      (tree-case s
        (lambda () z)
        (lambda (x) (c x z))
        (lambda (s~ l r)
          (digit-foldl c
                       (sequence-foldl (lambda (n acc)
                                         (node-foldl c acc n))
                                       (digit-foldl c z l)
                                       (force s~))
                       r))))

    (define (sequence-foldr c z s)
      (tree-case s
        (lambda () z)
        (lambda (x) (c x z))
        (lambda (s~ l r)
          (digit-foldr c
                       (sequence-foldr (lambda (n acc)
                                         (node-foldr c acc n))
                                       (digit-foldr c z r)
                                       (force s~))
                       l))))

    (define (sequence-map f s)
      (let loop ((s s))
        (tree-case s
          sequence-empty
          (lambda (x)
            (sequence-single (f x)))
          (lambda (s~ l r)
            (make-tree-deep (delay
                              (sequence-map (lambda (n)
                                              (node-map f n))
                                            (force s~)))
                            (digit-map f l)
                            (digit-map f r))))))

    (define (sequence-cons x s)
      (tree-case s
        (lambda ()
          ;; insert onto empty as leaf
          (make-tree-leaf x))
        (lambda (y)
          ;; insert as left digit and move leaf to right digit
          (make-tree-deep (delay (make-tree-nil))
                          (vector x)
                          (vector y)))
        (lambda (s~ l r)
          ;; insert onto left digit, rebalance and push spillover to deep branch
          (let-values (((l n) (digit-cons x l)))
            (make-tree-deep (if n
                                (delay (sequence-cons n (force s~)))
                                s~)
                            l
                            r)))))

    (define (sequence-snoc x s)
      (tree-case s
        (lambda ()
          ;; insert onto empty as leaf
          (make-tree-leaf x))
        (lambda (y)
          ;; insert as right digit and move leaf to left digit
          (make-tree-deep (delay (make-tree-nil))
                          (vector y)
                          (vector x)))
        (lambda (s~ l r)
          ;; insert onto right digit, rebalance and push spillover to deep branch
          (let-values (((r n) (digit-snoc x r)))
            (make-tree-deep (if n
                                (delay (sequence-snoc n (force s~)))
                                s~)
                            l
                            r)))))

    (define-record-type &sequence-empty-error
      (make-sequence-empty-error)
      sequence-empty-error?)

    (define (sequence-uncons s)
      (tree-case s
        (lambda ()
          (raise (make-sequence-empty-error)))
        (lambda (x)
          (values x (delay (sequence-empty))))
        (lambda (s~ l r)
          (let-values (((x l) (digit-uncons l)))
            (values x
                    (delay
                      ;; left may now be empty; invoke helper
                      (make-sequence-left s~ l r)))))))

    (define (sequence-unsnoc s)
      (tree-case s
        (lambda ()
          (raise (make-sequence-empty-error)))
        (lambda (x)
          (values x (delay (sequence-empty))))
        (lambda (s~ l r)
          (let-values (((x r) (digit-unsnoc r)))
            (values x
                    (delay
                      ;; right may now be empty; invoke helper
                      (make-sequence-right s~ l r)))))))

    (define (make-sequence-left s~ l r)
      (if l
          (make-tree-deep s~ l r)
          (if (tree-nil? (force s~))
              (digit->sequence r)
              (let-values (((l s~) (sequence-uncons (force s~))))
                (make-tree-deep s~ l r)))))

    (define (make-sequence-right s~ l r)
      (if r
          (make-tree-deep s~ l r)
          (if (tree-nil? (force s~))
              (digit->sequence l)
              (let-values (((r s~) (sequence-unsnoc (force s~))))
                (make-tree-deep s~ l r)))))

    (define (digit->sequence t)
      (digit-foldl sequence-snoc (sequence-empty) t))

    (define (list->sequence l)
      (fold sequence-snoc (sequence-empty) l))

    (define (reverse-list->sequence l)
      (fold sequence-cons (sequence-empty) l))

    (define (sequence->list s)
      (sequence-foldr cons '() s))

    (define (sequence->reverse-list s)
      (sequence-foldl cons '() s))

    (define (sequence-take s n)
      (let loop ((acc (sequence-empty))
                 (s s)
                 (n n))
        (cond ((<= n 0)
               acc)
              ((sequence-empty? s)
               (raise (make-sequence-empty-error)))
              (else
               (let-values (((x s~) (sequence-uncons s)))
                 (loop (sequence-snoc x acc)
                       (force s~)
                       (- n 1)))))))

    (define (sequence-drop s n)
      (cond ((<= n 0)
             s)
            ((sequence-empty? s)
             (raise (make-sequence-empty-error)))
            (else
             (let-values (((_ s~) (sequence-uncons s)))
               (sequence-drop (force s~)
                              (- n 1))))))

    (define (sequence-length s)
      (sequence-foldl (lambda (_ n) (+ n 1)) 0 s))

    (define (make-sequence n x)
      (let ((zero (delay (make-tree-nil)))
            (one (delay (make-vector 1 x)))
            (two (delay (make-vector 2 x)))
            (three (delay (make-vector 3 x))))
        (cond
         ((= n 0) (force zero))
         ((= n 1) (make-tree-leaf x))
         ((= n 2) (make-tree-deep zero (force one) (force one)))
         ((= n 3) (make-tree-deep zero (force two) (force one)))
         ((= n 4) (make-tree-deep zero (force two) (force two)))
         ((= n 5) (make-tree-deep zero (force three) (force two)))
         ((= n 6) (make-tree-deep zero (force three) (force three)))
         (else
          (let ((b (force three)))
            (let-values (((q r) (floor/ n 3)))
              (cond
               ((= r 0)
                (make-tree-deep (delay (make-sequence (- q 2) b))
                                (force three)
                                (force three)))
               ((= r 1)
                (make-tree-deep (delay (make-sequence (- q 1) b))
                                (force two)
                                (force two)))
               (else
                (make-tree-deep (delay (make-sequence (- q 1) b))
                                (force three)
                                (force two))))))))))

    ;; internal nodes of 2-3 elements

    (define (node-case t f2 f3)
      (if (= 2 (vector-length t))
          (f2 (vector-ref t 0)
              (vector-ref t 1))
          (f3 (vector-ref t 0)
              (vector-ref t 1)
              (vector-ref t 2))))

    (define (node-foldl c z t)
      (node-case t
        (lambda (v0 v1)
          (c v1 (c v0 z)))
        (lambda (v0 v1 v2)
          (c v2 (c v1 (c v0 z))))))

    (define (node-foldr c z t)
      (node-case t
        (lambda (v0 v1)
          (c v0 (c v1 z)))
        (lambda (v0 v1 v2)
          (c v0 (c v1 (c v2 z))))))

    (define (node-map f t)
      (node-case t
        (lambda (v0 v1)
          (vector (f v0)
                  (f v1)))
        (lambda (v0 v1 v2)
          (vector (f v0)
                  (f v1)
                  (f v2)))))

    ;; exterior digits of 1-4 elements

    (define (digit-case t f1 f2 f3 f4)
      (let ((n (vector-length t)))
        (cond ((= n 1)
               (f1 (vector-ref t 0)))
              ((= n 2)
               (f2 (vector-ref t 0)
                   (vector-ref t 1)))
              ((= n 3)
               (f3 (vector-ref t 0)
                   (vector-ref t 1)
                   (vector-ref t 2)))
              (else
               (f4 (vector-ref t 0)
                   (vector-ref t 1)
                   (vector-ref t 2)
                   (vector-ref t 3))))))

    (define (digit-foldl c z t)
      (digit-case t
        (lambda (v0)
          (c v0 z))
        (lambda (v0 v1)
          (c v1 (c v0 z)))
        (lambda (v0 v1 v2)
          (c v2 (c v1 (c v0 z))))
        (lambda (v0 v1 v2 v3)
          (c v3 (c v2 (c v1 (c v0 z)))))))

    (define (digit-foldr c z t)
      (digit-case t
        (lambda (v0)
          (c v0 z))
        (lambda (v0 v1)
          (c v0 (c v1 z)))
        (lambda (v0 v1 v2)
          (c v0 (c v1 (c v2 z))))
        (lambda (v0 v1 v2 v3)
          (c v0 (c v1 (c v2 (c v3 z)))))))

    (define (digit-map f t)
      (digit-case t
        (lambda (v0)
          (vector (f v0)))
        (lambda (v0 v1)
          (vector (f v0) (f v1)))
        (lambda (v0 v1 v2)
          (vector (f v0) (f v1) (f v2)))
        (lambda (v0 v1 v2 v3)
          (vector (f v0) (f v1) (f v2) (f v3)))))

    (define (digit-cons x t)
      (digit-case t
        (lambda (v0)
          (values (vector x v0)
                  #f))
        (lambda (v0 v1)
          (values (vector x v0 v1)
                  #f))
        (lambda (v0 v1 v2)
          (values (vector x v0 v1 v2)
                  #f))
        (lambda (v0 v1 v2 v3)
          (values (vector x v0)
                  (vector v1 v2 v3)))))

    (define (digit-snoc x t)
      (digit-case t
        (lambda (v0)
          (values (vector v0 x)
                  #f))
        (lambda (v0 v1)
          (values (vector v0 v1 x)
                  #f))
        (lambda (v0 v1 v2)
          (values (vector v0 v1 v2 x)
                  #f))
        (lambda (v0 v1 v2 v3)
          (values (vector v3 x)
                  (vector v0 v1 v2)))))

    (define (digit-uncons t)
      (digit-case t
        (lambda (v0)
          (values v0 #f))
        (lambda (v0 v1)
          (values v0 (vector v1)))
        (lambda (v0 v1 v2)
          (values v0 (vector v1 v2)))
        (lambda (v0 v1 v2 v3)
          (values v0 (vector v1 v2 v3)))))

    (define (digit-unsnoc t)
      (digit-case t
        (lambda (v0)
          (values v0 #f))
        (lambda (v0 v1)
          (values v1 (vector v0)))
        (lambda (v0 v1 v2)
          (values v2 (vector v0 v1)))
        (lambda (v0 v1 v2 v3)
          (values v3 (vector v0 v1 v2)))))))
