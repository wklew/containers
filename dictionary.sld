;;; dictionary.sld --- Efficient, purely functional integer-keyed maps

;;; Copyright © 2021, 2022 Walter Lewis <wklew@mailbox.org>
;;;
;;; This file is part of containers.
;;;
;;; Containers is free software; you can redistribute it and/or modify it
;;; under the terms of the GNU General Public License as published by
;;; the Free Software Foundation; either version 3 of the License, or (at
;;; your option) any later version.
;;;
;;; Containers is distributed in the hope that it will be useful, but
;;; WITHOUT ANY WARRANTY; without even the implied warranty of
;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;;; GNU General Public License for more details.
;;;
;;; You should have received a copy of the GNU General Public License
;;; along with containers.  If not, see <http://www.gnu.org/licenses/>.

(define-library (containers dictionary)

  (export dictionary-empty
          dictionary-empty?
          dictionary-single
          alist->dictionary
          dictionary-insert
          dictionary-insert-pair
          dictionary-lookup
          dictionary-lookup-error?
          dictionary-minimum
          dictionary-maximum
          dictionary-delete
          dictionary-decompose
          dictionary-merge
          dictionary-merge-list
          dictionary-merge-with
          dictionary-map
          dictionary-foldl
          dictionary-foldr
          dictionary->ascending-alist
          dictionary->descending-alist)

  (import (scheme base)
          (containers dictionary internal)))
