;;; tree.sld --- Binary tree interface

;;; Copyright © 2021, 2022 Walter Lewis <wklew@mailbox.org>
;;;
;;; This file is part of containers.
;;;
;;; Containers is free software; you can redistribute it and/or modify it
;;; under the terms of the GNU General Public License as published by
;;; the Free Software Foundation; either version 3 of the License, or (at
;;; your option) any later version.
;;;
;;; Containers is distributed in the hope that it will be useful, but
;;; WITHOUT ANY WARRANTY; without even the implied warranty of
;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;;; GNU General Public License for more details.
;;;
;;; You should have received a copy of the GNU General Public License
;;; along with containers.  If not, see <http://www.gnu.org/licenses/>.

(define-library (containers tree)

  (export make-tree-nil
          tree-nil?
          make-tree-leaf
          tree-leaf?
          tree-leaf-data
          make-tree-deep
          tree-deep?
          tree-deep-flag
          tree-deep-left
          tree-deep-right
          tree?
          tree-case
          tree-case2
          tree-deep)

  (import (scheme base))

  (begin

    (define-record-type <tree-nil>
      (make-tree-nil)
      tree-nil?)

    (define-record-type <tree-leaf>
      (make-tree-leaf val)
      tree-leaf?
      (val tree-leaf-data))

    (define-record-type <tree-deep>
      (make-tree-deep flag left right)
      tree-deep?
      (flag tree-deep-flag)
      (left tree-deep-left)
      (right tree-deep-right))

    (define (tree? x)
      (or (tree-nil? x)
          (tree-leaf? x)
          (tree-deep? x)))

    ;; Case split

    (define (tree-case tree nil leaf deep)
      (cond
       ((tree-nil? tree) (nil))
       ((tree-leaf? tree)
        (leaf (tree-leaf-data tree)))
       (else
        (deep (tree-deep-flag tree)
              (tree-deep-left tree)
              (tree-deep-right tree)))))

    (define (tree-case2 tree1 tree2 nil1 nil2 leaf1 leaf2 deep)
      (cond
       ((tree-nil? tree1) (nil1 tree2))
       ((tree-nil? tree2) (nil2 tree1))
       ((tree-leaf? tree1)
        (leaf1 (tree-leaf-data tree1)
               tree2))
       ((tree-leaf? tree2)
        (leaf2 (tree-leaf-data tree2)
               tree1))
       (else
        (deep (tree-deep-flag tree1)
              (tree-deep-left tree1)
              (tree-deep-right tree1)
              (tree-deep-flag tree2)
              (tree-deep-left tree2)
              (tree-deep-right tree2)))))

    ;; Smart constructor

    (define (tree-deep t l r)
      (cond
       ((tree-nil? l) r)
       ((tree-nil? r) l)
       (else (make-tree-deep t l r))))))
