# sequence

```scheme
(import (containers sequence))
```

Efficient, purely functional double-ended queues.

Sequences support fast access to the head and the tail,
as well as fast appends.
Additionally, interior values are computed lazily;
this leads to performance improvements on otherwise linear operations
like `make-sequence`, since they are performed incrementally.

Implementation based on:
Ralf Hinze and Ross Paterson,
"Finger trees: a simple general-purpose data structure",
Journal of Functional Programming 16:2 (2006) pp 197-217.

## contents

- [construction](#construction)
  - [empty](#empty)
  - [single](#single)
  - [from list](#from-list)
  - [from value](#from-value)
- [querying](#querying)
  - [empty?](#empty)
- [insertion and removal](#insertion-and-removal)
  - [cons](#cons)
  - [uncons](#uncons)
- [appending](#appending)
  - [append](#append)
- [mapping](#mapping)
  - [map](#map)
- [folding](#folding)
  - [fold](#fold)
  - [to list](#to-list)

# construction

## empty

```scheme
(sequence-empty)
```

_O(1)._
Create an empty sequence.

## single

```scheme
(sequence-single key val)
```

_O(1)._
Create a singleton sequence.

## from list

```scheme
(list->sequence lst)
(reverse-list->sequence lst)
```

_O(n)._
Create a sequence from `lst`, in forward or reverse order.

## from value

```scheme
(make-sequence count x)
```

_O(log(n))._
Create a sequence consisting of `count` copies of `x`.

# querying

## empty?

```scheme
(sequence-empty? seq)
```

_O(1)._
Test whether `seq` is empty.

# insertion and removal

## cons

```scheme
(sequence-cons a seq)
(sequence-snoc z seq)
```

_O(1)._
Insert `a` or `z` onto `seq` as the first or last element, respectively.
Return the new sequence.

## uncons

```scheme
(sequence-uncons seq)
(sequence-unsnoc seq)
```

_O(1)._
Remove the first or last element from `seq`.
Return as two values the removed element
and a promise to evaluate the remaining sequence.
If `seq` is empty, raise an exception satisfying `sequence-empty-error?`.

Since the remaining sequence is computed lazily,
there is no performance penalty for using this function
in cases where only the first value is needed.
Otherwise, remember to force the second value.

```scheme
(define (find-from-left x seq)
  (guard (err
          ((sequence-empty-error? err)
           (display "not found!\n")))
    (let loop ((i 0) (seq seq))
      (let-values (((y rest) (sequence-uncons seq)))
        (if (eq? x y)
            i
            (loop (+ i 1)
                  (force rest)))))))

(find-from-left 'x (list->sequence '(a b c x y)))
;; => 3
```

# appending

## append

```scheme
(sequence-append seq seq* ...)
```

_O(log(min(n,m)))._
Append the sequences from left to right.

# mapping

## map

```scheme
(sequence-map proc seq)
```

_O(n)._
Map over values in `seq`, applying `proc` to each element.
Mapping over internal elements is performed incrementally.

# folding

## fold

```scheme
(sequence-foldl combine base seq)
(sequence-foldr combine base seq)
```

_O(n)._
Fold over values in `seq`, from left-to-right or right-to-left,
using an insertion function and base value.
Return the final value accumulated by the fold.

## to list

```scheme
(sequence->list seq)
(sequence->reverse-list seq)
```

_O(n)._
Convert `seq` to a list, in forward or reverse order.
