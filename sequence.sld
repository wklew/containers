;;; sequence.sld --- Efficient, purely functional double-ended queues

;;; Copyright © 2021, 2022 Walter Lewis <wklew@mailbox.org>
;;;
;;; This file is part of containers.
;;;
;;; Containers is free software; you can redistribute it and/or modify it
;;; under the terms of the GNU General Public License as published by
;;; the Free Software Foundation; either version 3 of the License, or (at
;;; your option) any later version.
;;;
;;; Containers is distributed in the hope that it will be useful, but
;;; WITHOUT ANY WARRANTY; without even the implied warranty of
;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;;; GNU General Public License for more details.
;;;
;;; You should have received a copy of the GNU General Public License
;;; along with containers.  If not, see <http://www.gnu.org/licenses/>.

(define-library (containers sequence)

  (export sequence?
          sequence-empty
          sequence-empty?
          sequence-empty-error?
          sequence-single
          sequence-cons
          sequence-snoc
          sequence-uncons
          sequence-unsnoc
          sequence-foldl
          sequence-foldr
          sequence-map
          list->sequence
          make-sequence
          reverse-list->sequence
          sequence->list
          sequence->reverse-list
          sequence-take
          sequence-drop
          sequence-length
          sequence-append)

  (import (scheme base)
          (containers sequence internal)
          (containers sequence append))

  (cond-expand
   (guile (import (srfi srfi-1)))
   (chibi (import (srfi 1))))

  (begin

    ;; Define top-level append here, to avoid circular dependencies

    (define (sequence-append . ss)
      (fold (lambda (s acc)
              (%sequence-append acc s))
            (sequence-empty)
            ss))))
