;;; dictionary/bitmask.sld --- Fast bitwise operations

;;; Copyright © 2021, 2022 Walter Lewis <wklew@mailbox.org>
;;;
;;; This file is part of containers.
;;;
;;; Containers is free software; you can redistribute it and/or modify it
;;; under the terms of the GNU General Public License as published by
;;; the Free Software Foundation; either version 3 of the License, or (at
;;; your option) any later version.
;;;
;;; Containers is distributed in the hope that it will be useful, but
;;; WITHOUT ANY WARRANTY; without even the implied warranty of
;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;;; GNU General Public License for more details.
;;;
;;; You should have received a copy of the GNU General Public License
;;; along with containers.  If not, see <http://www.gnu.org/licenses/>.

(define-library (containers dictionary bitmask)

  (export zero-bit?
          mask
          match-prefix?
          branch-mask)

  (import (scheme base))

  (cond-expand

   (guile
    (import (srfi srfi-60)
            (rnrs arithmetic fixnums (6)))
    (begin
     (define key-size (fixnum-width))))

   (chibi
    (import (srfi 142) (srfi 143))
    (begin
      (define key-size fx-width))))

  (begin

    (define (zero-bit? key depth)
      (fxzero? (fxand key depth)))

    (define (mask key depth)
      (fxand (fxior key (fx- depth 1))
             (fxnot depth)))

    (define (match-prefix? key pre depth)
      (fx=? pre (mask key depth)))

    (define (highest-bit-mask x)
      (fxarithmetic-shift-left 1 (fx- (fxlength x) 1)))

    (define (branch-mask pre pre*)
      (highest-bit-mask (fxxor pre pre*)))))
