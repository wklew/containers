;;; dictionary/internal.sld --- Internal dictionary definitions

;;; Copyright © 2021, 2022 Walter Lewis <wklew@mailbox.org>
;;;
;;; This file is part of containers.
;;;
;;; Containers is free software; you can redistribute it and/or modify it
;;; under the terms of the GNU General Public License as published by
;;; the Free Software Foundation; either version 3 of the License, or (at
;;; your option) any later version.
;;;
;;; Containers is distributed in the hope that it will be useful, but
;;; WITHOUT ANY WARRANTY; without even the implied warranty of
;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;;; GNU General Public License for more details.
;;;
;;; You should have received a copy of the GNU General Public License
;;; along with containers.  If not, see <http://www.gnu.org/licenses/>.

;;; Commentary:
;;;
;;; Implementation taken from the following sources:
;;;
;;; - Chris Okasaki and Andy Gill, "Fast Mergeable Integer Maps",
;;;   Workshop on ML, September 1998, pages 77-86,
;;;   http://citeseerx.ist.psu.edu/viewdoc/summary?doi=10.1.1.37.5452
;;;
;;; - Haskell's Data.IntMap (Daan Leijen and Andriy Palamarchuk)

(define-library (containers dictionary internal)

  (export dictionary-empty
          dictionary-empty?
          dictionary-single
          alist->dictionary
          dictionary-insert
          dictionary-insert-pair
          dictionary-lookup
          dictionary-lookup-error?
          dictionary-minimum
          dictionary-maximum
          dictionary-delete
          dictionary-decompose
          dictionary-merge
          dictionary-merge-list
          dictionary-merge-with
          dictionary-map
          dictionary-foldl
          dictionary-foldr
          dictionary->ascending-alist
          dictionary->descending-alist)

  (import (scheme base)
          (scheme case-lambda)
          (containers tree)
          (containers dictionary bitmask))

  (cond-expand
   (guile (import (srfi srfi-1)))
   (chibi (import (srfi 1))))

  (begin

    ;; Useful higher order functions

    (define (replace _ x) x)

    (define (flip f)
      (lambda (x y)
        (f y x)))

    ;; Helper: does key i diverge from prefix p, up to depth m?

    (define (diverging? i p m)
      (not (match-prefix? i p m)))

    ;; Entries in the dictionary, stored in the leaves of the tree.
    ;; For parity with alists, we actually store them as cons cells.
    ;; This saves us a few operations when we convert to/from alists.

    (define make-entry cons)
    (define entry-key car)
    (define entry-value cdr)

    ;; Labels attached to the internal nodes of the tree,
    ;; containing an integer prefix and bitmask of the current depth.

    (define-record-type <label>
      (make-label p m)
      label?
      (p label-prefix)
      (m label-depth))

    (define dictionary-empty make-tree-nil)
    (define dictionary-empty? tree-nil?)

    (define (dictionary-single i x)
      (make-tree-leaf (make-entry i x)))

    (define-record-type &dictionary-lookup-error
      (make-dictionary-lookup-error)
      dictionary-lookup-error?)

    (define (dictionary-lookup-error)
      (raise
       (make-dictionary-lookup-error)))

    (define (dictionary-lookup i t)
      (let loop ((t t))
        (tree-case t
          dictionary-lookup-error
          (lambda (e)
            (if (= i (entry-key e))
                (entry-value e)
                (dictionary-lookup-error)))
          (lambda (l t0 t1)
            (let ((p (label-prefix l))
                  (m (label-depth l)))
              (cond ((diverging? i p m)
                     (dictionary-lookup-error))
                    ((<= i p)
                     (loop t0))
                    (else
                     (loop t1))))))))

    (define (destructure-entry e)
      (values (entry-key e)
              (entry-value e)))

    (define (dictionary-minimum t)
      (tree-case t
        dictionary-lookup-error
        destructure-entry
        (lambda (l t _)
          (dictionary-minimum t))))

    (define (dictionary-maximum t)
      (tree-case t
        dictionary-lookup-error
        destructure-entry
        (lambda (l _ t)
          (dictionary-maximum t))))

    (define (dictionary-delete i t)
      (let loop ((t t))
        (tree-case t
          make-tree-nil
          (lambda (e)
            (if (= i (entry-key e))
                (make-tree-nil)
                (make-tree-leaf e)))
          (lambda (l t0 t1)
            (let ((p (label-prefix l))
                  (m (label-depth l)))
              (cond ((diverging? i p m)
                     t)
                    ((<= i p)
                     (tree-deep l (loop t0) t1))
                    (else
                     (tree-deep l t0 (loop t1)))))))))

    (define (dictionary-decompose i t)
      (let loop ((t t))
        (tree-case t
          dictionary-lookup-error
          (lambda (e)
            (if (= i (entry-key e))
                (values (entry-value e)
                        (make-tree-nil))
                (dictionary-lookup-error)))
          (lambda (l t0 t1)
            (let ((p (label-prefix l))
                  (m (label-depth l)))
              (cond ((diverging? i p m)
                     (dictionary-lookup-error))
                    ((<= i p)
                     (let-values (((x t0) (loop t0)))
                       (values x (tree-deep l t0 t1))))
                    (else
                     (let-values (((x t1) (loop t1)))
                       (values x (tree-deep l t0 t1))))))))))

    (define (dictionary-join p q s t)
      (let ((m (branch-mask p q)))
        (apply make-tree-deep
               (make-label (mask p m) m)
               (if (zero-bit? p m)
                   (list s t)
                   (list t s)))))

    (define (%dictionary-insert combine e t)
      (let loop ((t t))
        (tree-case t
          (lambda ()
            (make-tree-leaf e))
          (lambda (f)
            (let ((i (entry-key e))
                  (j (entry-key f)))
              (if (= i j)
                  (dictionary-single i (combine (entry-value f)
                                                (entry-value e)))
                  (dictionary-join i j (make-tree-leaf e) t))))
          (lambda (l t0 t1)
            (let ((i (entry-key e))
                  (p (label-prefix l))
                  (m (label-depth l)))
              (cond ((diverging? i p m)
                     (dictionary-join i p (make-tree-leaf e) t))
                    ((<= i p)
                     (make-tree-deep l (loop t0) t1))
                    (else
                     (make-tree-deep l t0 (loop t1)))))))))

    (define dictionary-insert-pair
      (case-lambda
        ((e t)
         (%dictionary-insert replace e t))
        ((combine e t)
         (%dictionary-insert combine e t))))

    (define dictionary-insert
      (case-lambda
        ((i x t)
         (dictionary-insert replace i x t))
        ((combine i x t)
         (%dictionary-insert combine (make-entry i x) t))))

    (define (%dictionary-merge combine s t)
      (let loop ((s s)
                 (t t))
        (tree-case2 s t
          (lambda (t) t)
          (lambda (s) s)
          (lambda (e t)
            (%dictionary-insert (flip combine) e t))
          (lambda (e s)
            (%dictionary-insert combine e s))
          (lambda (k s0 s1 l t0 t1)
            (let ((p (label-prefix k))
                  (q (label-prefix l))
                  (m (label-depth k))
                  (n (label-depth l)))
              (cond
               ((and (= m n) (= p q))
                ;; The trees have the same prefix. Merge the subtrees.
                (make-tree-deep k (loop s0 t0) (loop s1 t1)))
               ((and (> m n) (match-prefix? q p m))
                ;; q contains p. Merge t with a subtree of s.
                (if (zero-bit? q m)
                    (make-tree-deep k (loop s0 t) s1)
                    (make-tree-deep k s0 (loop s1 t))))
               ((and (< m n) (match-prefix? p q n))
                ;; p contains q. Merge s with a subtree of t.
                (if (zero-bit? p n)
                    (make-tree-deep l (loop s t0) t1)
                    (make-tree-deep l t0 (loop s t1))))
               (else
                ;; The prefixes disagree.
                (dictionary-join p q s t))))))))

    (define (dictionary-merge-list combine dicts)
      (fold (lambda (t acc)
              (%dictionary-merge combine acc t))
            (dictionary-empty)
            dicts))

    (define (dictionary-merge-with combine . dicts)
      (dictionary-merge-list combine dicts))

    (define (dictionary-merge . dicts)
      (dictionary-merge-list replace dicts))

    (define (dictionary-map proc t)
      (let loop ((t t))
        (tree-case t
          make-tree-nil
          (lambda (e)
            (make-tree-leaf
             (make-entry (entry-key e)
                         (proc (entry-value e)))))
          (lambda (l t0 t1)
            (make-tree-deep l (loop t0) (loop t1))))))

    (define (%dictionary-foldl insert empty t)
      (let loop ((acc empty)
                 (t t))
        (tree-case t
          (lambda () acc)
          (lambda (e)
            (insert e acc))
          (lambda (_ t0 t1)
            (loop (loop acc t0) t1)))))

    (define (%dictionary-foldr insert empty t)
      (let loop ((acc empty)
                 (t t))
        (tree-case t
          (lambda () acc)
          (lambda (e)
            (insert e acc))
          (lambda (_ t0 t1)
            (loop (loop acc t1) t0)))))

    (define (dictionary-foldl insert empty t)
      (%dictionary-foldl (lambda (e acc)
                           (insert (entry-key e)
                                   (entry-value e)
                                   acc))
                         empty
                         t))

    (define (dictionary-foldr insert empty t)
      (%dictionary-foldr (lambda (e acc)
                           (insert (entry-key e)
                                   (entry-value e)
                                   acc))
                         empty
                         t))

    (define (dictionary->ascending-alist t)
      (%dictionary-foldr cons '() t))

    (define (dictionary->descending-alist t)
      (%dictionary-foldl cons '() t))

    (define alist->dictionary
      (case-lambda
        ((alist)
         (alist->dictionary replace alist))
        ((combine alist)
         (fold (lambda (e t)
                 (%dictionary-insert combine e t))
               (dictionary-empty)
               alist))))
    ))
