;;; Copyright © 2021, 2022 Walter Lewis <wklew@mailbox.org>
;;;
;;; This file is part of containers.
;;;
;;; Containers is free software; you can redistribute it and/or modify it
;;; under the terms of the GNU General Public License as published by
;;; the Free Software Foundation; either version 3 of the License, or (at
;;; your option) any later version.
;;;
;;; Containers is distributed in the hope that it will be useful, but
;;; WITHOUT ANY WARRANTY; without even the implied warranty of
;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;;; GNU General Public License for more details.
;;;
;;; You should have received a copy of the GNU General Public License
;;; along with containers.  If not, see <http://www.gnu.org/licenses/>.

(import (scheme base)
        (scheme write)
        (containers dictionary))

(cond-expand
  (chibi (import (chibi test)))
  (guile (import (srfi srfi-64))))

(test-begin "dictionary")

(define d1
  (alist->dictionary
   '((100 x)
     (200 y)
     (1400 z))))

(define d2
  (alist->dictionary
   '((2 a)
     (3 b)
     (5 c))))

(define d3
  (dictionary-single 100 '(u)))

(define d4
  (dictionary-insert 5 '(d) (dictionary-insert 100 '(v) (dictionary-empty))))

(define d5
  (dictionary-merge-with append d1 d2 d3 d4))

(test-assert
  (equal? (dictionary-lookup 5 d5)
          '(c d)))

(test-assert
  (equal? (dictionary-lookup 100 d5)
          '(x u v)))

(test-assert
  (equal? (dictionary->ascending-alist d5)
          '((2 a)
            (3 b)
            (5 c d)
            (100 x u v)
            (200 y)
            (1400 z))))

(test-end "dictionary")
