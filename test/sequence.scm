;;; Copyright © 2021, 2022 Walter Lewis <wklew@mailbox.org>
;;;
;;; This file is part of containers.
;;;
;;; Containers is free software; you can redistribute it and/or modify it
;;; under the terms of the GNU General Public License as published by
;;; the Free Software Foundation; either version 3 of the License, or (at
;;; your option) any later version.
;;;
;;; Containers is distributed in the hope that it will be useful, but
;;; WITHOUT ANY WARRANTY; without even the implied warranty of
;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;;; GNU General Public License for more details.
;;;
;;; You should have received a copy of the GNU General Public License
;;; along with containers.  If not, see <http://www.gnu.org/licenses/>.

(cond-expand
  (chibi (import (chibi test)))
  (guile
   (import (srfi srfi-64))
   (begin
     (define (test x y)
       (test-equal x y)))))

(import (containers sequence)
        (scheme base)
        (scheme lazy))

;;; Quick utility

(define (pred n)
  (- n 1))

(define (snoc x lst)
  (append lst `(,x)))

(define (iota n)
  (let loop ((n n) (lst '()))
    (if (zero? n)
        lst
        (let ((n (pred n)))
          (loop n (cons n lst))))))

;;; Lazy destructuring in keeping with the sequences implementation

(define-record-type &list-empty-error
  (make-list-empty-error)
  list-empty-error?)

(define (uncons lst)
  (if (null? lst)
      (raise (make-list-empty-error))
      (values (car lst)
              (delay (cdr lst)))))

(define (unsnoc lst)
  (if (null? lst)
      (raise (make-list-empty-error))
      (let ((lst (reverse lst)))
        (values (car lst)
                (delay (reverse (cdr lst)))))))

;;; Iteratively build up or tear down a sequence and a list in parallel,
;;; testing for equality as we go

(define (test-ana done? lst-plus seq-plus next seed)
  (let loop ((lst '())
             (seq (sequence-empty))
             (seed seed))
    (test lst (sequence->list seq))
    (if (done? seed)
        (values)
        (loop (lst-plus seed lst)
              (seq-plus seed seq)
              (next seed)))))

(define (test-cata lst-minus seq-minus lst seq)
  (let loop ((lst lst)
             (seq seq))
    ;; an error terminates the test
    (guard (exc
            ((sequence-empty-error? exc) (values))
            ((list-empty-error? exc) (values)))
      (let-values (((la ld) (lst-minus lst))
                   ((sa sd) (seq-minus seq)))
        (test la sa)
        (loop (force ld)
              (force sd))))))

;;; Run the tests

(test-begin "sequence")

(let ((n 50))
  (test-group "sequence-cons"
    (test-ana zero? cons sequence-cons pred 49))
  (test-group "sequence-snoc"
    (test-ana zero? snoc sequence-snoc pred 49))
  (let* ((lst (iota n))
         (seq (list->sequence lst)))
    (test-group "sequence-uncons"
      (test-cata uncons sequence-uncons lst seq))
    (test-group "sequence-unsnoc"
      (test-cata unsnoc sequence-unsnoc lst seq))))

(test-group "sequence-append"
  (let ((lst1 (iota 120))
        (lst2 (iota 39)))
    (test
     (append lst1 lst2)
     (sequence->list
      (sequence-append (list->sequence lst1)
                       (list->sequence lst2)))))
  (let ((lst1 (iota 48))
        (lst2 (iota 101)))
    (test
     (append lst1 lst2)
     (sequence->list
      (sequence-append (list->sequence lst1)
                       (list->sequence lst2)))))
  (test
   (sequence->list
    (apply sequence-append
           (map list->sequence
                '((a b c d e f g)
                  (h i j k)
                  (l m n o p)
                  (q r s t u)
                  (v w)
                  (x y z)))))
   '(a b c d e f g h i j k l m n o p q r s t u v w x y z)))

(test-group "make-sequence"
  (test
   (make-list 47 #f)
   (sequence->list (make-sequence 47 #f)))
  (test
   (make-list 236 #f)
   (sequence->list (sequence-map not (make-sequence 236 #t)))))

(test-end "sequence")
